var tac_logs = "/var/log/tac_plus"
Tail = require('tail').Tail;
const es = require('./elasticsearch.js');
const os = require('os');

const matchAll = require("match-all");


const ip_regex = /(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/;

function getFiles(dir, filelist = null) {
    var path = path || require('path');
    var fs = fs || require('fs'), files = fs.readdirSync(dir);
    var filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = getFiles(path.join(dir, file), filelist);
        }
        else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

var tail_monitors = [];
var monitored_files = [];

function monitor() {
    var file_list = getFiles(tac_logs);
    file_list.forEach(file => {
        // If there is no entry for a file create one and initialize the newline monitor.
        if (!monitored_files.includes(file)) {
            console.log("[l] Monitoring new log file '" + file + "'");
            monitored_files.push(file);
            tail = new Tail(file);
            tail.on("line", function (data) {
                data = "" + data;
                console.log("[l] TAC_PLUS LOG EVENT: <" + data + ">");
                var category = "accounting";
                if (file.includes("access")) {
                    category = "access";
                }
                if (file.includes("authentication")) {
                    category = "authentication";
                }
                
                var ips = matchAll(data, ip_regex);
                es.log_put(source=os.hostname(), category=category, src=(ips[0] || "0.0.0.0"), dst=(ips[1] || "0.0.0.0"), log_data=data);
            });
            tail_monitors.push(tail_monitors);
        }
    });
}

var state = null;
function run_monitor() {
    if (!state) {
        console.log("[i] Initializing log collector...");
        state = setInterval(function () {
            monitor();
        }, 1000);
    }
}

module.exports = {
    // Start the log monitor.
    initialize: function () {
        run_monitor();
    }
}
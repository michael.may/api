## Contributing to the Project
Currently contribution guidelines are simple
- **Fork** the repository
- Submit a **Merge Request** and wait for approval.

I aim to respond to any merge requests within 48 hours.